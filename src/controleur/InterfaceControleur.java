package controleur;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import modele.Dimensions;
import modele.Ligne;
import modele.Station;
import modele.Horaire;
import modele.Position;
import vue.ecran.Ecran;

public interface InterfaceControleur {
	public void lancer();

	public void ajouterStation(Position position, String nom_francais,
			String nom_anglais, String nom_cyrillique);

	public void ajouterLigne(String nom, LinkedList<Station> stations, ArrayList<Horaire> horaires);

	public HashMap<Integer, Station> getStations();

	public HashMap<Integer, Ligne> getLignes();

	public Dimensions getDimensions();

	public void chargerModele(String chemin) throws IOException;

	public boolean modeleCharge();

	public void setEcran(Ecran ecran);

	public AbstractTableModel getListeStations();

	void sauvegarderModele();
}
