package controleur;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import modele.Dimensions;
import modele.InterfaceModele;
import modele.Ligne;
import modele.Station;
import modele.Horaire;
import modele.Position;
import vue.InterfaceVue;
import vue.ecran.Ecran;

public class ControleurPrincipal implements InterfaceControleur {

	InterfaceModele modele;

	InterfaceVue vue;

	/**
	 * Constructeur par défaut du ControleurPrincipal
	 * 
	 * @param modele
	 *            Le modèle à controler
	 * @param vue
	 *            La vue qui affiche le modèle
	 */
	public ControleurPrincipal(InterfaceModele modele, InterfaceVue vue) {
		this.modele = modele;
		this.vue = vue;
		vue.ajouterControleur(this);
	}

	@Override
	public void lancer() {
		vue.lancer();
	}

	@Override
	public void ajouterStation(Position position, String nom_francais,
			String nom_anglais, String nom_cyrillique) {
		modele.ajouterStation(position, nom_francais, nom_anglais,
				nom_cyrillique);
	}

	@Override
	public void ajouterLigne(String nom, LinkedList<Station> stations, ArrayList<Horaire> horaires) {
		modele.ajouterLigne(nom, stations, horaires);
	}

	@Override
	public HashMap<Integer, Station> getStations() {
		return modele.getStations();
	}

	@Override
	public HashMap<Integer, Ligne> getLignes() {
		return modele.getLignes();
	}

	@Override
	public Dimensions getDimensions() {
		return modele.getDimensionsModele();
	}

	@Override
	public void chargerModele(String chemin) throws IOException {
		modele.chargerModele(chemin);
	}

	@Override
	public boolean modeleCharge() {
		return modele.modeleCharge();
	}

	@Override
	public void setEcran(Ecran ecran) {
		vue.modifierEcran(ecran);
	}

	@Override
	public AbstractTableModel getListeStations() {
		return modele.getListeStations();
	}

	@Override
	public void sauvegarderModele() {
		try {
			modele.enregistrerModele("recherches/stations.txt");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
