package modele;

import java.util.ArrayList;
import java.util.LinkedList;

public class Ligne {
	private Integer id;
	private String nom;
	private LinkedList<Station> stations;
  private ArrayList<Horaire> horaires;

	/**
	 * Constructeur de la classe ligne.
	 * 
	 * @param id
	 *            : identifiant de la ligne
	 * @param nom
	 *            : nom de la ligne
	 * @param stations
	 *            : Liste des stations qui appartiennent à la ligne
	 */
	public Ligne(Integer id, String nom, LinkedList<Station> stations, ArrayList<Horaire> horaires) {
		this.id = id;
		this.nom = nom;
		this.stations = stations;
    this.horaires = horaires;
	}

	/**
	 * Retourne l'identifiant de la station.
	 * 
	 * @return id : identifiant de la station
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Retourne le nom de la station.
	 * 
	 * @return nom : nom de la station
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Retourne la liste des stations de la ligne.
	 * 
	 * @return stations : Liste chainée des stations de la ligne
	 */
	public LinkedList<Station> getStations() {
		return stations;
	}
  
	/**
	 * Retourne la liste des horaires de la ligne.
	 * 
	 * @return horaires : Liste des horaires de la ligne
	 */
	public ArrayList<Horaire> getHoraires() {
		return horaires;
	}

	/**
	 * Créer un tableau contenant les identifiants des stations de la ligne et
	 * retourne ce tableau.
	 * 
	 * @return tab.toArray(new Integer[tab.size()]) : tableau des identifiants
	 *         des stations de la ligne
	 */
	public Integer[] getTableauIdStations() {
		ArrayList<Integer> tab = new ArrayList<Integer>();

		for (int i = 0; i < stations.size(); i++) {
			tab.add(stations.get(i).getId());
		}

		return tab.toArray(new Integer[tab.size()]);
	}

}
