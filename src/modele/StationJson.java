package modele;

class StationJson {
	private int id;
	private String nom_francais;
	private String nom_anglais;
	private String nom_cyrillique;
	private Position position;

	/**
	 * Retourne l'identifiant de la station
	 * 
	 * @return id : identifiant de la station
	 */
	public int getId() {
		return id;
	}

	/**
	 * Retourne le nom français de la station
	 * 
	 * @return nom_francais : nom français de la station
	 */
	public String getNom_francais() {
		return nom_francais;
	}

	/**
	 * Retourne le nom anglais de la station
	 * 
	 * @return nom_anglais : nom anglais de la station
	 */
	public String getNom_anglais() {
		return nom_anglais;
	}

	/**
	 * Retourne le nom cyrillique de la station
	 * 
	 * @return nom_cyrillique : nom cyrillique de la station
	 */
	public String getNom_cyrillique() {
		return nom_cyrillique;
	}

	/**
	 * Retourne la position de la station
	 * 
	 * @return position : position de la station
	 */
	public Position getPosition() {
		return position;
	}
}
