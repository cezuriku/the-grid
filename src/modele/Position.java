package modele;

public class Position {
	private double longitude;
	private double latitude;

	/**
	 * Constructeur de la classe position.
	 * 
	 * @param longitude
	 *            : longitude de la position
	 * @param latitude
	 *            : latitude de la position
	 */
	public Position(double longitude, double latitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
	}

	/**
	 * Constructeur de copie de la classe position.
	 * 
	 * @param p
	 *            : La position à copier
	 */
	public Position(Position p) {
		this.longitude = p.longitude;
		this.latitude = p.latitude;
	}

	/**
	 * Constructeur vide de la classe position.
	 * 
	 */
	public Position() {
		this.longitude = 0.0d;
		this.latitude = 0.0d;
	}

	/**
	 * Retourne la longitude.
	 * 
	 * @return longitude : coordonnée géographique
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Modifie la longitude.
	 * 
	 * @param longitude
	 *            : longitude de la Position
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Retourne la lattitude.
	 * 
	 * @return lattitude : coordonnée géographique
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Modifie la latitude.
	 * 
	 * @param latitude
	 *            : latitude de la Position
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Ajoute 2 positions
	 * 
	 * @param p1
	 *            : la première position
	 * @param p2
	 *            : la seconde position
	 */
	public static Position somme(Position p1, Position p2) {
		Position total = new Position(p1);
		total.longitude += p2.longitude;
		total.latitude += p2.latitude;
		return total;
	}

	/**
	 * Soustrait 2 positions
	 * 
	 * @param p1
	 *            : la première position
	 * @param p2
	 *            : la seconde position
	 */
	public static Position soustraction(Position p1, Position p2) {
		Position total = new Position(p1);
		total.longitude -= p2.longitude;
		total.latitude -= p2.latitude;
		return total;
	}

}
