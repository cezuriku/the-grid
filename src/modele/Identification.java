package modele;

public class Identification {
	private static Integer dernierIdDonne = null;

	/**
	 * Retourne un nouvel identifiant unique
	 * 
	 * @return id : identifiant unique retourné
	 */
	public Integer getId() {
		if (dernierIdDonne == null) {
			dernierIdDonne = 0;
			return dernierIdDonne;
		} else
			return ++dernierIdDonne;
	}

	/**
	 * Modifie le dernier identifiant donné
	 * 
	 * @param id
	 *            : le nouveau dernier identifiant donné
	 */
	public void setDernierIdDonne(Integer id) {
		dernierIdDonne = id;
	}

}
