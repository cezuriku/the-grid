package modele;

public class Horaire {
	private Integer debut;
  private Integer fin;
  private Integer frequence;

	/**
	 * Constructeur de la classe Horaire.
	 * 
	 * @param debut
	 *            : heure de début de l'horaire
	 * @param fin
	 *            : heure de fin de l'horaire
	 * @param frequence
	 *            : fréquence de passage
	 */
	public Horaire(Integer debut, Integer fin, Integer frequence) {
		this.debut = debut;
    this.fin = fin;
    this.frequence = frequence;
	}

	/**
	 * Retourne l'heure de début de l'horaire
	 * 
	 * @return heure de début de l'horaire
	 */
	public Integer getDebut() {
		return debut;
	}
  
	/**
	 * Retourne l'heure de fin de l'horaire
	 * 
	 * @return heure de fin de l'horaire
	 */
	public Integer getFin() {
		return fin;
	}
  
	/**
	 * Retourne la fréquence de passage
	 * 
	 * @return fréquence de passage
	 */
	public Integer getFrequence() {
		return frequence;
	}
}
