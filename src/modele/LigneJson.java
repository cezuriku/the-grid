package modele;

import java.util.ArrayList;

class LigneJson {
	private Integer id;
	private String nom;
	private Integer[] idStations;
  private ArrayList<Horaire> horaires;

	/**
	 * Constructeur de la classe LigneJson.
	 * 
	 * @param id
	 *            : identifiant de la ligne
	 * @param nom
	 *            : nom de la ligne
	 * @param idStations
	 *            : ids des stations appartenant à la ligne
	 */
	public LigneJson(Integer id, String nom, Integer[] idStations, ArrayList<Horaire> horaires) {
		this.id = id;
		this.nom = nom;
		this.idStations = idStations;
    this.horaires = horaires;
	}

	/**
	 * Retourne l'identifiant de la ligne
	 * 
	 * @return id : identifiant de la ligne
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Retourne le nom de la ligne
	 * 
	 * @return nom : nom de la ligne
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Retourne les identifiants des stations apprtenant à la ligne
	 * 
	 * @return idStations : identifiants des stations appartenant à la ligne
	 */
	public Integer[] getIdStations() {
		return idStations;
	}
  
	/**
	 * Retourne la liste des horaires de la ligne.
	 * 
	 * @return horaires : Liste des horaires de la ligne
	 */
	public ArrayList<Horaire> getHoraires() {
		return horaires;
	}
}
