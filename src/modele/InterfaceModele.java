package modele;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public interface InterfaceModele {
	/**
	 * Ajoute une station au modèle
	 * 
	 * @param position
	 *            : position de la station
	 * @param nom_francais
	 *            : nom français de la station
	 * @param nom_anglais
	 *            : nom anglais de la station
	 * @param nom_cyrillique
	 *            : nom cyrillique de la station
	 */
	public void ajouterStation(Position position, String nom_francais,
			String nom_anglais, String nom_cyrillique);

	/**
	 * Ajoute une ligne au modèle
	 * 
	 * @param nom
	 *            : nom de la ligne
	 * @param stations
	 *            : stations à associer à la ligne
	 */
	public void ajouterLigne(String nom, LinkedList<Station> stations, ArrayList<Horaire> horaires);

	/**
	 * Supprime une station du modèle
	 * 
	 * @param id
	 *            : identifiant de la station à supprimer
	 */
	public void supprimerStation(Integer id);

	/**
	 * Supprime une ligne du modèle
	 * 
	 * @param id
	 *            : identifiant de la ligne à supprimer
	 */
	public void supprimerLigne(Integer id);

	/**
	 * Retourne les dimensions du modele
	 * 
	 * @return Les dimensions du modèle
	 */
	public Dimensions getDimensionsModele();

	/**
	 * Retourne les stations du modèle
	 * 
	 * @return stations : stations du modèle
	 */
	public HashMap<Integer, Station> getStations();

	/**
	 * Retourne les lignes du modèle
	 * 
	 * @return lignes : lignes du modèle
	 */
	public HashMap<Integer, Ligne> getLignes();

	/**
	 * Charge le modèle à partir d'un fichier JSON
	 * 
	 * @param chemin
	 *            : chemin vers le fichier JSON à charger
	 */
	public void chargerModele(String chemin) throws IOException;

	/**
	 * Sauvegarde le modèle à partir d'un fichier JSON
	 * 
	 * @param chemin
	 *            : chemin vers le fichier JSON sur lequel sauvegarder
	 */
	public void enregistrerModele(String chemin) throws IOException;

	/**
	 * Retourne vrai si le modèle a été chargé, faux sinon
	 * 
	 * @return modeleChargeBoolean : boolean valant vrai si le modèle a été
	 *         chargé, faux sinon
	 */
	public boolean modeleCharge();

	public AbstractTableModel getListeStations();
}
