package modele;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.*;

import javax.swing.table.AbstractTableModel;

import com.google.gson.Gson;

public class Carte implements InterfaceModele {
	private Identification identificationStations;
	private Identification identificationLignes;

	private HashMap<Integer, Station> stations;
	private HashMap<Integer, Ligne> lignes;

	private Dimensions dimensions;

	private boolean charge = false;

	@SuppressWarnings("serial")
	public class ListeStations extends AbstractTableModel {

		/**
		 * Constructeur de la classe ListeStations
		 */
		public ListeStations() {
		}

		@Override
		public int getColumnCount() {
			return Station.getColonnes().length;
		}

		@Override
		public Object getValueAt(int ligne, int colonne) {
			switch (this.getColumnName(colonne)) {
			case "Position(latitude)":
				return stations.get(ligne).getPosition().getLatitude();

			case "Position(longitude)":
				return stations.get(ligne).getPosition().getLongitude();

			case "Nom français":
				return stations.get(ligne).getNom_francais();

			case "Nom anglais":
				return stations.get(ligne).getNom_anglais();

			case "Nom cyrillique":
				return stations.get(ligne).getNom_cyrillique();

			default:
				return null;

			}
		}

		@Override
		public int getRowCount() {
			return stations.size();
		}

		@Override
		public String getColumnName(int colonne) {
			return Station.getColonnes()[colonne];
		}

		@Override
		public boolean isCellEditable(int ligne, int colonne) {
			return true;
		}

		@Override
		public void setValueAt(Object v, int ligne, int colonne) {

			switch (this.getColumnName(colonne)) {
			case "Position(latitude)":
				stations.get(ligne).getPosition()
						.setLatitude(Double.parseDouble((String) v));
				break;

			case "Position(longitude)":
				stations.get(ligne).getPosition()
						.setLongitude(Double.parseDouble((String) v));
				break;

			case "Nom français":
				stations.get(ligne).setNom_francais((String) v);
				break;

			case "Nom anglais":
				stations.get(ligne).setNom_anglais((String) v);
				break;

			case "Nom cyrillique":
				stations.get(ligne).setNom_cyrillique((String) v);
				break;

			default:
				break;

			}
		}
	}

	/**
	 * Constructeur de la classe Carte
	 * 
	 */
	public Carte() {
		identificationStations = new Identification();
		identificationLignes = new Identification();

		stations = new HashMap<Integer, Station>();
		lignes = new HashMap<Integer, Ligne>();
	}

	@Override
	public boolean modeleCharge() {
		return charge;
	}

	/**
	 * Ajoute une station à la carte
	 * 
	 * @param id
	 *            : id de la future station
	 * @param station
	 *            : la station à ajouter
	 */
	private void ajouterStation(Integer id, Station station) {
		// Si aucun identifiant n'est donné, on le génère
		if (id == null)
			id = identificationStations.getId();
		// Sinon, on indique à l'identification que c'est celui-ci le dernier id
		// donné
		else
			identificationStations.setDernierIdDonne(id);

		stations.put(id, station);
	}

	@Override
	public void ajouterLigne(String nom, LinkedList<Station> stations, ArrayList<Horaire> horaires) {
		ajouterLigne(null, new Ligne(null, nom, stations, horaires));
	}

	/**
	 * Ajoute une ligne à la carte
	 * 
	 * @param id
	 *            : id de laligne
	 * @param ligne
	 *            : ligne à ajouter
	 */
	private void ajouterLigne(Integer id, Ligne ligne) {
		// Si aucun identifiant n'est donné, on le génère
		if (id == null)
			id = identificationLignes.getId();
		// Sinon, on indique à l'identification que c'est celui-ci le dernier id
		// donné
		else
			identificationLignes.setDernierIdDonne(id);

		lignes.put(id, ligne);
	}

	@Override
	public void ajouterStation(Position position, String nom_francais,
			String nom_anglais, String nom_cyrillique) {
		ajouterStation(null, new Station(null, position, nom_francais,
				nom_anglais, nom_cyrillique));
	}

	@Override
	public void supprimerStation(Integer id) {
		stations.remove(id);
	}

	@Override
	public void supprimerLigne(Integer id) {
		lignes.remove(id);
	}

	@Override
	public void chargerModele(String chemin) throws IOException {
		// Chargement du fichier et stockage dans une String
		byte[] encoded = Files.readAllBytes(Paths.get(chemin));
		String json = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded))
				.toString();

		// Dé-sérialisation des données Json dans la classe adaptée
		Gson gson = new Gson();
		DonneesJson donnees = gson.fromJson(json, DonneesJson.class);

		// Initialisation des dimensions à partir des données dé-séralisées
		dimensions = new Dimensions(donnees.getPositionMinimum(),
				donnees.getPositionMaximum());

		if (donnees.getStations() != null) {
			// Les stations
			for (int i = 0; i < donnees.getStations().size(); i++) {
				ajouterStation(donnees.getStations().get(i).getId(), donnees
						.getStations().get(i));
			}

			if (donnees.getLignes() != null) {
				// Les lignes
				for (int i = 0; i < donnees.getLignes().size(); i++) {
					LinkedList<Station> listeDeStations = new LinkedList<Station>();

					for (int j = 0; j < donnees.getLignes().get(i)
							.getIdStations().length; j++) {
						listeDeStations.add(stations.get(donnees.getLignes()
								.get(i).getIdStations()[j]));
					}

					ajouterLigne(donnees.getLignes().get(i).getId(), new Ligne(
							donnees.getLignes().get(i).getId(), donnees.getLignes().get(i).getNom(),
							listeDeStations, donnees.getLignes().get(i).getHoraires()));
				}
			}
		}

		charge = true;
	}

	@Override
	public void enregistrerModele(String chemin) throws IOException {
		// Création de la variable donneesJson qui contiendra nos données
		DonneesJson donneesJson;

		// Récupération des dimensions
		donneesJson = new DonneesJson(getDimensionsModele()
				.getPositionMinimum(), getDimensionsModele()
				.getPositionMaximum());

		// Ajout des stations
		for (Map.Entry<Integer, Station> entry : stations.entrySet()) {
			donneesJson.ajouterStation(entry.getValue());
		}

		// Ajout des lignes
		for (Map.Entry<Integer, Ligne> entry : lignes.entrySet()) {
			donneesJson.ajouterLigne(new LigneJson(entry.getKey(), entry
					.getValue().getNom(), entry.getValue()
					.getTableauIdStations(), entry.getValue()
					.getHoraires()));
		}

		// Dé-sérialisation de l'objet donneesJson dans une variable json de
		// type String
		Gson gson = new Gson();
		String json = gson.toJson(donneesJson);

		// On écrit dans le fichier
		FileWriter writer = null;
		try {
			writer = new FileWriter(chemin);
			writer.write(json, 0, json.length());
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	@Override
	public Dimensions getDimensionsModele() {
		return dimensions;
	}

	@Override
	public HashMap<Integer, Station> getStations() {
		return stations;
	}

	@Override
	public HashMap<Integer, Ligne> getLignes() {
		return lignes;
	}

	@Override
	public AbstractTableModel getListeStations() {
		return new ListeStations();
	}
}
