package modele;

public class Dimensions {
	private Position positionMinimum;
	private Position positionMaximum;

	/**
	 * Constructeur de la classe Dimensions.
	 * 
	 * @param positionMinimum
	 *            : borne inférieure
	 * @param positionMaximum
	 *            : borne supérieure
	 */
	public Dimensions(Position positionMinimum, Position positionMaximum) {
		this.positionMinimum = positionMinimum;
		this.positionMaximum = positionMaximum;
	}

	/**
	 * Retourne la borne inférieure
	 * 
	 * @return positionMinimum : borne inférieure
	 */
	public Position getPositionMinimum() {
		return positionMinimum;
	}

	/**
	 * Modifie la borne inférieure
	 * 
	 * @param positionMinimum
	 *            : borne inférieure
	 */
	public void setPositionMinimum(Position positionMinimum) {
		this.positionMinimum = positionMinimum;
	}

	/**
	 * Retourne la borne supérieure de la Position.
	 * 
	 * @return positionMaximum : Borne supérieure de la Position
	 */
	public Position getPositionMaximum() {
		return positionMaximum;
	}

	/**
	 * Modifie la borne supérieure
	 * 
	 * @param positionMaximum
	 *            : borne supérieure
	 */
	public void setPositionMaximum(Position positionMaximum) {
		this.positionMaximum = positionMaximum;
	}

	/**
	 * Retourne la taille des dimensions
	 * 
	 * @return position : taille
	 */
	public Position getTaille() {
		return new Position(positionMaximum.getLongitude()
				- positionMinimum.getLongitude(), positionMaximum.getLatitude()
				- positionMinimum.getLatitude());
	}
}
