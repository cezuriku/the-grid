package modele;

public class Station {
	private Integer id;
	private Position position;
	private String nom_francais;
	private String nom_anglais;
	private String nom_cyrillique;

	private static final String[] colonnes = { "Position(latitude)",
			"Position(longitude)", "Nom français", "Nom anglais",
			"Nom cyrillique" };

	/**
	 * Constructeur de la classe Station.
	 * 
	 * @param id
	 *            : identifiant de la station
	 * @param position
	 *            : position de la station
	 * @param nom_francais
	 *            : traduction française du nom de la station
	 * @param nom_anglais
	 *            : traduction anglaise du nom de la station
	 * @param nom_cyrillique
	 *            : nom initial de la station
	 */
	public Station(Integer id, Position position, String nom_francais,
			String nom_anglais, String nom_cyrillique) {
		this.id = id;
		this.position = position;
		this.nom_francais = nom_francais;
		this.nom_anglais = nom_anglais;
		this.nom_cyrillique = nom_cyrillique;
	}

	/**
	 * Retourne les noms des différents attributs de Station
	 * 
	 * @return le nom de ses attributs
	 */
	public static String[] getColonnes() {
		return colonnes;
	}

	/**
	 * Retourne les coordonées géographiques de la station.
	 * 
	 * @return position de l'objet
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * Modifie les coordonnées géographiques de la station.
	 * 
	 * @param position
	 *            : coordonnées géographiques à donner à la station
	 */
	public void setPosition(Position position) {
		this.position = position;
	}

	/**
	 * Retourne le nom français de la station.
	 * 
	 * @return nom_français : le nom de la station traduit en français
	 */
	public String getNom_francais() {
		return nom_francais;
	}

	/**
	 * Modifie le nom français de la station.
	 * 
	 * @param nom_francais
	 *            traduction française à donner au nom de la station
	 */
	public void setNom_francais(String nom_francais) {
		this.nom_francais = nom_francais;
	}

	/**
	 * Retourne le nom anglais de la station.
	 * 
	 * @return nom_anglais : le nom de la station traduit en anglais
	 */
	public String getNom_anglais() {
		return nom_anglais;
	}

	/**
	 * Modifie le nom anglais de la station.
	 * 
	 * @param nom_anglais
	 *            traduction anglaise à donner au nom de la station
	 */
	public void setNom_anglais(String nom_anglais) {
		this.nom_anglais = nom_anglais;
	}

	/**
	 * Retourne le nom initial de la station.
	 * 
	 * @return nom_cyrillique : nom initial de la station
	 */
	public String getNom_cyrillique() {
		return nom_cyrillique;
	}

	/**
	 * Modifie le nom initial de la station.
	 * 
	 * @param nom_cyrillique
	 *            : nom initial de la station à modifié
	 */
	public void setNom_cyrillique(String nom_cyrillique) {
		this.nom_cyrillique = nom_cyrillique;
	}

	/**
	 * Retourne l'identifiant de la station
	 * 
	 * @return id : identifiant de la station
	 */
	public int getId() {
		return id;
	}

	public Station clone() {
		return new Station(id, position, nom_francais, nom_anglais,
				nom_cyrillique);
	}
}
