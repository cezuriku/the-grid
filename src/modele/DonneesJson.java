package modele;

import java.util.ArrayList;

class DonneesJson {
	private Position positionMinimum;
	private Position positionMaximum;

	private ArrayList<Station> stations;

	private ArrayList<LigneJson> lignes;

	/**
	 * Constructeur de la classe DonneesJson.
	 * 
	 * @param positionMinimum
	 *            : position minimum d'une station des données
	 * @param positionMaximum
	 *            : position maximum d'une station des données
	 */
	public DonneesJson(Position positionMinimum, Position positionMaximum) {
		this.positionMinimum = positionMinimum;
		this.positionMaximum = positionMaximum;
		stations = new ArrayList<Station>();
		lignes = new ArrayList<LigneJson>();
	}

	/**
	 * Retourne la position minimum
	 * 
	 * @return positionMinimum : position minimum
	 */
	public Position getPositionMinimum() {
		return positionMinimum;
	}

	/**
	 * Retourne la position maximum
	 * 
	 * @return positionMaximum : position maximum
	 */
	public Position getPositionMaximum() {
		return positionMaximum;
	}

	/**
	 * Retourne les stations
	 * 
	 * @return stations : les stations
	 */
	public ArrayList<Station> getStations() {
		return stations;
	}

	/**
	 * Retourne les lignes
	 * 
	 * @return lignes : les lignes
	 */
	public ArrayList<LigneJson> getLignes() {
		return lignes;
	}

	/**
	 * Ajoute une station aux données
	 * 
	 * @param station
	 *            : la station à ajouter
	 */
	public void ajouterStation(Station station) {
		stations.add(station);
	}

	/**
	 * Ajoute une ligne aux données
	 * 
	 * @param ligneJson
	 *            : la ligne à ajouter
	 */
	public void ajouterLigne(LigneJson ligneJson) {
		lignes.add(ligneJson);
	}
}
