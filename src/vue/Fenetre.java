package vue;

import javax.swing.JFrame;

import modele.Carte;
import modele.InterfaceModele;
import vue.ecran.Ecran;
import vue.ecran.EcranAffichageStation;
import controleur.ControleurPrincipal;
import controleur.InterfaceControleur;

public class Fenetre extends JFrame implements InterfaceVue {

	private static final long serialVersionUID = 4311226428752637699L;
	private InterfaceControleur controleur;
	private Ecran ecran;

	/**
	 * Constructeur par défaut de la fenetre
	 */
	public Fenetre() {
		super();

		this.setTitle("TheGrid");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void ajouterControleur(InterfaceControleur controleur) {
		this.controleur = controleur;
	}

	@Override
	public void lancer() {
		//modifierEcran(new EcranMenu(controleur, this.getContentPane()));
		modifierEcran(new EcranAffichageStation(controleur, this.getContentPane()));
	}

	@Override
	public void modifierEcran(Ecran ecran) {
		if(this.ecran != null)
			this.ecran.supprimerListeners();
		this.ecran = ecran;
		this.revalidate();
		this.repaint();
		this.setVisible(true);
	}

	public static void main(String[] args) {
		InterfaceModele modele = new Carte();
		InterfaceVue vue = new Fenetre();
		InterfaceControleur controleur = new ControleurPrincipal(modele, vue);
		
		controleur.lancer();
	}

}
