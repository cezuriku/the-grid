package vue.panel;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import modele.Position;
import vue.bouton.BoutonLien;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
/**
 * @class PanelAjouterStation
 * 
 */
public class PanelAjouterStation extends JPanel {	
    String nomCyrillique = new String();
    String nomAnglais = new String();
    String nomFrancais = new String();
    String longitude = new String();
    String latitude = new String();
    
    JTextField tfNomCyrillique = new JTextField(15);
    JTextField tfNomAnglais = new JTextField(15);
    JTextField tfNomFrancais = new JTextField(15);
    JTextField tfLongitude = new JTextField(15);
    JTextField tfLatitude = new JTextField(15);
    
    /**
     * Constructeur de la classe PanelAjouterStation
     * Crée un formulaire permettant d'ajouter une station
     * @param controleur
     * @param conteneur
     */
	public PanelAjouterStation(InterfaceControleur controleur,
			Container conteneur) {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setBackground(new Color(0, 8, 8));
		JLabel taTitre = new JLabel("Ajouter une station");
		taTitre.setForeground(new Color(87, 146, 245));
		JLabel taNomCyrillique = new JLabel("Entrez le nom cyrillique : ");
		taNomCyrillique.setForeground(new Color(87, 146, 245));
		JLabel taNomAnglais = new JLabel("Entrez le nom anglais : ");
		taNomAnglais.setForeground(new Color(87, 146, 245));
        JLabel taNomFrancais = new JLabel("Entrez le nom français : ");
        taNomFrancais.setForeground(new Color(87, 146, 245));
        JLabel taLongitude = new JLabel("Entrez la longitude : ");
        taLongitude.setForeground(new Color(87, 146, 245));
        JLabel taLatitude = new JLabel("Entrez la latitude : ");
        taLatitude.setForeground(new Color(87, 146, 245));
		
        Font font = new Font("Arial", Font.BOLD, 17);
		taTitre.setFont(font);
        
        // Panel du titre
        JPanel panelTitre = new JPanel();
        panelTitre.setLayout(new FlowLayout(1, 10, 25));
        panelTitre.setOpaque(false);
        panelTitre.add(taTitre);
        
        // Champs pour le nom en français
        JPanel panelNomFrancais = new JPanel();
        panelNomFrancais.setLayout(new FlowLayout(1, 10, 0));
        panelNomFrancais.setOpaque(false);
        panelNomFrancais.add(taNomFrancais);
        panelNomFrancais.add(tfNomFrancais);
        
        // Champs pour le nom en anglais
        JPanel panelNomAnglais = new JPanel();
        panelNomAnglais.setLayout(new FlowLayout(1, 10, 0));
        panelNomAnglais.setOpaque(false);
        panelNomAnglais.add(taNomAnglais);
        panelNomAnglais.add(tfNomAnglais);
        
        // Champs pour le nom en cyrillique
        JPanel panelNomCyrillique = new JPanel();
        panelNomCyrillique.setLayout(new FlowLayout(1, 10, 0));
        panelNomCyrillique.setOpaque(false);
        panelNomCyrillique.add(taNomCyrillique);
        panelNomCyrillique.add(tfNomCyrillique);
        
        // Champs pour la latitude
        JPanel panelLatitude = new JPanel();
        panelLatitude.setLayout(new FlowLayout(1, 10, 0));
        panelLatitude.setOpaque(false);
        panelLatitude.add(taLatitude);
        panelLatitude.add(tfLatitude);
        
        // Champs pour la longitude
        JPanel panelLongitude = new JPanel();
        panelLongitude.setLayout(new FlowLayout(1, 10, 0));
        panelLongitude.setOpaque(false);
        panelLongitude.add(taLongitude);
        panelLongitude.add(tfLongitude);
        
        // Bouton valider
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout(1, 20, 0));
        panelValider.setOpaque(false);
        BoutonValider valider = new BoutonValider(controleur, conteneur);
        panelValider.add(valider);
        
        this.add(panelTitre);
        this.add(panelNomFrancais);
        this.add(panelNomAnglais);
        this.add(panelNomCyrillique);
        this.add(panelLatitude);
        this.add(panelLongitude);
        this.add(panelValider);
     }
	
	/**
	 * Classe BoutonValider qui permet d'ajouter une station
	 */
	 private class BoutonValider extends BoutonLien {
		private static final long serialVersionUID = -2841834147669139212L;

		/**
		 * Constructeur du bouton de validation pour ll'ajout de station
		 * @param controleur
		 * @param conteneur
		 */
		public BoutonValider(InterfaceControleur controleur, Container conteneur) {
			super(controleur, conteneur);
			this.setText("Valider");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
	        nomFrancais = tfNomFrancais.getText();
	        nomAnglais = tfNomAnglais.getText();
			nomCyrillique = tfNomCyrillique.getText();
	        latitude = tfLatitude.getText();
	        longitude = tfLongitude.getText();
	        
	        if(nomFrancais.isEmpty() || nomAnglais.isEmpty() || nomCyrillique.isEmpty() || latitude.isEmpty() || longitude.isEmpty()){
	        	System.out.println("C'est vide !");
	        } else{
	        	Position position = new Position(Double.parseDouble(longitude), Double.parseDouble(latitude));
	        	controleur.ajouterStation(position, nomFrancais, nomAnglais, nomCyrillique);
	        }
		}
	}
}
