package vue.panel;

import java.awt.Color;
import java.awt.Container;

import javax.swing.JPanel;

import vue.bouton.BoutonLienAffichageListeStations;
import vue.bouton.BoutonLienAffichageStation;
import vue.bouton.BoutonLienAjouterStation;
import vue.bouton.BoutonSauvegarder;
import controleur.InterfaceControleur;

/**
 * Cette classe représente un panel pouvant servir de menu entre les différents
 * panels a voir
 * 
 */
@SuppressWarnings("serial")
public class PanelBoutonMenu extends JPanel {

	/**
	 * Constructeur par défaut
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public PanelBoutonMenu(InterfaceControleur controleur, Container conteneur) {
		this.setBackground(new Color(0, 8, 8));
		this.add(new BoutonLienAffichageStation(controleur, conteneur));
		// this.add(new BoutonLienAffichageLigne(controleur, conteneur));
		this.add(new BoutonLienAjouterStation(controleur, conteneur));
		this.add(new BoutonLienAffichageListeStations(controleur, conteneur));
		this.add(new BoutonSauvegarder(controleur));
	}

}
