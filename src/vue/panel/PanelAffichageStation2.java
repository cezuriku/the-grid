package vue.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.Font;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.ArrayList;

import javax.swing.JPanel;

import modele.Dimensions;
import modele.Ligne;
import modele.Position;
import modele.Station;
import modele.Horaire;
import vue.panel.formes.Carre;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class PanelAffichageStation2 extends JPanel implements MouseListener,
		MouseMotionListener, ComponentListener {

	private static final int TAILLE = 5;
	private InterfaceControleur controleur;
	private boolean aInitialiser;
	private double minX;
	private double minY;
	private Position taille;
	private Carre stationMouvement;
	private boolean stationEnMouvement;
	private Station stationAAfficher;
	private ArrayList<Color> couleursLignes;

	/**
	 * Constructeur par défaut
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 */
	public PanelAffichageStation2(InterfaceControleur controleur) {
		// On récupère le contrôleur et on initialise des variables
		this.controleur = controleur;
		this.aInitialiser = true;
		stationEnMouvement = false;

		// On remplit la liste des couleurs des lignes
		couleursLignes = new ArrayList<Color>();
		couleursLignes.add(new Color(87, 146, 245));
		couleursLignes.add(new Color(40, 247, 47));
		couleursLignes.add(new Color(255, 71, 218));
		couleursLignes.add(new Color(255, 255, 255));
		couleursLignes.add(new Color(252, 0, 17));
		couleursLignes.add(new Color(205, 23, 255));
		couleursLignes.add(new Color(63, 234, 252));
		couleursLignes.add(new Color(246, 252, 63));
		couleursLignes.add(new Color(255, 179, 48));
		couleursLignes.add(new Color(180, 255, 161));
		couleursLignes.add(new Color(200, 187, 242));
		couleursLignes.add(new Color(222, 204, 149));

		// On charge le modèle si ce n'est pas déjà fait
		if (!controleur.modeleCharge()) {
			try {
				controleur.chargerModele("recherches/stations.txt");
			} catch (IOException ex) {
				System.out.println(ex.toString());
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent souris) {
	}

	@Override
	public void mouseEntered(MouseEvent souris) {
	}

	@Override
	public void mouseExited(MouseEvent souris) {

	}

	@Override
	public void mousePressed(MouseEvent evenement) {
		Position pSouris = new Position(evenement.getX(), evenement.getY());
		Position pStation = new Position();
		Position pDistance = new Position();
		Station station;
		double tailleX = taille.getLongitude();
		double tailleY = taille.getLatitude();
		double x;
		double y;
		HashMap<Integer, Station> listesStations = controleur.getStations();

		for (Entry<Integer, Station> mapStation : listesStations.entrySet()) {
			station = mapStation.getValue();
			x = station.getPosition().getLongitude();
			y = station.getPosition().getLatitude();
			pStation.setLongitude((x - minX) / tailleX * this.getWidth());
			pStation.setLatitude((y - minY) / tailleY * this.getHeight());
			pDistance = Position.soustraction(pSouris, pStation);
			if (pDistance.getLatitude() >= 0 && pDistance.getLongitude() >= 0
					&& pDistance.getLatitude() < TAILLE * 2
					&& pDistance.getLongitude() < TAILLE * 2) {
				stationEnMouvement = true;
				stationMouvement = new Carre(station, new Position(
						evenement.getX() * tailleX / this.getWidth() + minX,
						evenement.getY() * tailleY / this.getHeight() + minY));
			}
		}
		this.repaint();
	}

	@Override
	public void componentResized(ComponentEvent e) {
		this.aInitialiser = true;
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent evenement) {
		if (stationEnMouvement == true) {
			stationEnMouvement = false;
			stationMouvement.valider();
			this.repaint();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (stationEnMouvement) {
			double tailleX = taille.getLongitude();
			double tailleY = taille.getLatitude();
			stationEnMouvement = true;
			stationMouvement.setPosition(new Position(e.getX() * tailleX
					/ this.getWidth() + minX, e.getY() * tailleY
					/ this.getHeight() + minY));
			this.repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent evenement) {
		Station ancienneStation = stationAAfficher;
		stationAAfficher = null;

		Position pSouris = new Position(evenement.getX(), evenement.getY());
		Position pStation = new Position();
		Position pDistance = new Position();
		Station station;
		double tailleX = taille.getLongitude();
		double tailleY = taille.getLatitude();
		double x;
		double y;
		HashMap<Integer, Station> listesStations = controleur.getStations();

		for (Entry<Integer, Station> mapStation : listesStations.entrySet()) {
			station = mapStation.getValue();
			x = station.getPosition().getLongitude();
			y = station.getPosition().getLatitude();
			pStation.setLongitude((x - minX) / tailleX * this.getWidth());
			pStation.setLatitude((y - minY) / tailleY * this.getHeight());
			pDistance = Position.soustraction(pSouris, pStation);
			if (pDistance.getLatitude() >= 0 && pDistance.getLongitude() >= 0
					&& pDistance.getLatitude() < TAILLE * 2
					&& pDistance.getLongitude() < TAILLE * 2) {
				stationAAfficher = station;
			}
		}

		if (stationAAfficher != ancienneStation)
			repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		// On déclare un entier qui contiendra l'id de la ligne à laquelle
		// appartient la station dont on doit afficher les informations sur le
		// côté
		int stationAAfficherLigneId = 0;

		if (aInitialiser)
			initialiserPanel();

		g.setColor(new Color(0, 8, 8));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		// Si le modèle est chargé...
		if (controleur.modeleCharge()) {
			HashMap<Integer, Ligne> listesLignes = controleur.getLignes();
			Dimensions dimensions = controleur.getDimensions();

			// On affiche les lignes
			for (Entry<Integer, Ligne> mapLigne : listesLignes.entrySet()) {
				// On stocke la ligne dans une variable intermédiaire
				Ligne ligne = mapLigne.getValue();

				// On choisit la couleur de la ligne en fonction de son id
				g.setColor(this.couleursLignes.get(ligne.getId()
						% this.couleursLignes.size()));

				// On récupère la première station
				LinkedList<Station> listesStations = ligne.getStations();
				Iterator<Station> stationIt = listesStations.iterator();
				Station station1 = stationIt.next();
				Station station2;

				if (stationAAfficher != null && stationAAfficher == station1)
					stationAAfficherLigneId = ligne.getId();

				while (stationIt.hasNext()) {
					station2 = stationIt.next();
					dessinerLigne(station1.getPosition(),
							station2.getPosition(), g);
					station1 = station2;
					if (stationAAfficher != null
							&& stationAAfficher == station1)
						stationAAfficherLigneId = ligne.getId();
				}
			}

			// On affiche la zone
			g.setColor(new Color(255, 255, 255));

			dessinerLigne(dimensions.getPositionMinimum(), new Position(
					dimensions.getPositionMaximum().getLongitude(), dimensions
							.getPositionMinimum().getLatitude()), g);
			dessinerLigne(dimensions.getPositionMinimum(), new Position(
					dimensions.getPositionMinimum().getLongitude(), dimensions
							.getPositionMaximum().getLatitude()), g);
			dessinerLigne(dimensions.getPositionMaximum(), new Position(
					dimensions.getPositionMaximum().getLongitude(), dimensions
							.getPositionMinimum().getLatitude()), g);
			dessinerLigne(dimensions.getPositionMaximum(), new Position(
					dimensions.getPositionMinimum().getLongitude(), dimensions
							.getPositionMaximum().getLatitude()), g);

			// On affiche les stations
			double tailleX = taille.getLongitude();
			double tailleY = taille.getLatitude();
			double x;
			double y;

			g.setColor(new Color(255, 255, 255));

			HashMap<Integer, Station> listesStations = controleur.getStations();

			for (Entry<Integer, Station> mapStation : listesStations.entrySet()) {
				Station station = mapStation.getValue();
				x = station.getPosition().getLongitude();
				y = station.getPosition().getLatitude();

				dessinerStation(station, g);
			}

			// On affiche la station en train d'être déplacée par l'utilisateur
			if (stationEnMouvement) {
				g.setColor(new Color(87, 146, 245));
				x = stationMouvement.getPosition().getLongitude();
				y = stationMouvement.getPosition().getLatitude();

				g.drawRect((int) ((x - minX) / tailleX * this.getWidth()),
						(int) ((y - minY) / tailleY * this.getHeight()),
						TAILLE * 2, TAILLE * 2);
			}

			if (stationAAfficher != null) {
				// On choisit la couleur du texte en fonction de l'id de la
				// ligne
				g.setColor(this.couleursLignes.get(stationAAfficherLigneId
						% this.couleursLignes.size()));

				// On affiche les infos de la station
				g.drawString(
						"Nom français : " + stationAAfficher.getNom_francais(),
						30, 100);
				g.drawString(
						"Nom anglais : " + stationAAfficher.getNom_anglais(),
						30, 120);
				g.drawString(
						"Nom cyrillique : "
								+ stationAAfficher.getNom_cyrillique(), 30, 140);
				g.drawString("Latitude : "
						+ stationAAfficher.getPosition().getLatitude(), 30, 160);
				g.drawString("Longitude : "
						+ stationAAfficher.getPosition().getLongitude(), 30,
						180);

				// On met une ligne de séparation
				g.drawLine(30, 200, 300, 200);

				// Et celles de la ligne à laquelle elle appartient
				g.drawString(
						listesLignes.get(stationAAfficherLigneId).getNom(), 30,
						230);

				// On affiche les horaires
				int yPosition = 0;
				g.drawString("Horaires : ", 30, 260);
				for (Horaire horaire : listesLignes
						.get(stationAAfficherLigneId).getHoraires()) {
					g.drawString(
							"De " + horaire.getDebut() + "h à "
									+ horaire.getFin() + "h : toutes les "
									+ horaire.getFrequence() + " minutes.", 50,
							280 + yPosition);
					yPosition += 20;
				}
			}

			// On affiche le titre
			g.setFont(new Font("Arial", Font.BOLD, 35));
			g.setColor(new Color(87, 146, 245));
			g.drawString("Plan du métro de Moscou", 30, 50);

			g.dispose();
		}

	}

	/**
	 * Permet de dessiner une station sur un Graphics
	 * 
	 * @param s
	 *            La station a dessiner
	 * @param g
	 *            Le Graphics sur lequel on dessine
	 */
	private void dessinerStation(Station s, Graphics g) {
		double x = s.getPosition().getLongitude();
		double y = s.getPosition().getLatitude();

		g.drawRect(
				(int) ((x - minX) / taille.getLongitude() * this.getWidth()),
				(int) ((y - minY) / taille.getLatitude() * this.getHeight()),
				2 * TAILLE, 2 * TAILLE);

	}

	/**
	 * Permet de dessiner une ligne entre 2 positions sur un Graphics
	 * 
	 * @param p1
	 *            La premiere position
	 * @param p2
	 *            La seconde position
	 * @param g
	 *            Le Graphics sur lequel on dessine
	 */
	private void dessinerLigne(Position p1, Position p2, Graphics g) {
		double x1 = p1.getLongitude();
		double y1 = p1.getLatitude();
		double x2 = p2.getLongitude();
		double y2 = p2.getLatitude();

		g.drawLine(
				(int) ((x1 - minX) / taille.getLongitude() * this.getWidth())
						+ TAILLE,
				(int) ((y1 - minY) / taille.getLatitude() * this.getHeight())
						+ TAILLE,
				(int) ((x2 - minX) / taille.getLongitude() * this.getWidth())
						+ TAILLE,
				(int) ((y2 - minY) / taille.getLatitude() * this.getHeight())
						+ TAILLE);
	}

	/**
	 * Permet d'initialiser ou reinitialiser le cadre a dessiner pour que tout
	 * le modèle soit vu sur le panel
	 */
	private void initialiserPanel() {
		Dimensions dim = controleur.getDimensions();
		double ratio = (double) this.getWidth() / (double) this.getHeight();

		minX = dim.getPositionMinimum().getLongitude();
		minY = dim.getPositionMinimum().getLatitude();
		taille = dim.getTaille();

		if (taille.getLongitude() > taille.getLatitude() * ratio) {
			minY -= (taille.getLongitude() / ratio - taille.getLatitude()) / 2;
			taille.setLatitude(taille.getLongitude() / ratio);
		} else {
			minX -= (taille.getLatitude() * ratio - taille.getLongitude()) / 2;
			taille.setLongitude(taille.getLatitude() * ratio);
		}
		repaint();
		aInitialiser = false;
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
	}

}
