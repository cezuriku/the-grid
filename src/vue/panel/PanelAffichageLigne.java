package vue.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import javax.swing.JPanel;

import modele.Dimensions;
import modele.Ligne;
import modele.Position;
import modele.Station;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class PanelAffichageLigne extends JPanel implements ComponentListener {
	private static final int TAILLE = 3;
	private InterfaceControleur controleur;
	private boolean aInitialiser;
	private double minX;
	private double minY;
	private Position taille;

	/**
	 * Constructeur par défaut
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 */
	public PanelAffichageLigne(InterfaceControleur controleur) {
		this.controleur = controleur;
		this.aInitialiser = true;

		if (!controleur.modeleCharge()) {
			try {
				controleur.chargerModele("recherches/stations.txt");
			} catch (IOException ex) {
				System.out.println(ex.toString());
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		if (aInitialiser)
			initialiserPanel();
		g.clearRect(0, 0, this.getWidth(), this.getHeight());

		if (controleur.modeleCharge()) {

			g.setColor(Color.BLACK);

			HashMap<Integer, Ligne> listesLignes = controleur.getLignes();

			for (Entry<Integer, Ligne> mapLigne : listesLignes.entrySet()) {
				Ligne ligne = mapLigne.getValue();
				LinkedList<Station> listesStations = ligne.getStations();
				Iterator<Station> stationIt = listesStations.iterator();
				Station station1 = stationIt.next();
				Station station2;
				dessinerStation(station1, g);

				while (stationIt.hasNext()) {
					station2 = stationIt.next();
					dessinerStation(station2, g);
					dessinerLigne(station1, station2, g);
					station1 = station2;
				}
			}

			g.dispose();
		}

	}

	/**
	 * Permet de dessiner une station sur un Graphics
	 * 
	 * @param s
	 *            La station a dessiner
	 * @param g
	 *            Le Graphics sur lequel on dessine
	 */
	private void dessinerStation(Station s, Graphics g) {
		double x = s.getPosition().getLongitude();
		double y = s.getPosition().getLatitude();

		g.drawRect(
				(int) ((x - minX) / taille.getLongitude() * this.getWidth()),
				(int) ((y - minY) / taille.getLatitude() * this.getHeight()),
				2 * TAILLE, 2 * TAILLE);

	}

	/**
	 * Permet de dessiner une ligne entre 2 stations sur un Graphics
	 * 
	 * @param s1
	 *            La premiere station
	 * @param s2
	 *            La seconde station
	 * @param g
	 *            Le Graphics sur lequel on dessine
	 */
	private void dessinerLigne(Station s1, Station s2, Graphics g) {
		double x1 = s1.getPosition().getLongitude();
		double y1 = s1.getPosition().getLatitude();
		double x2 = s2.getPosition().getLongitude();
		double y2 = s2.getPosition().getLatitude();

		g.drawLine(
				(int) ((x1 - minX) / taille.getLongitude() * this.getWidth())
						+ TAILLE,
				(int) ((y1 - minY) / taille.getLatitude() * this.getHeight())
						+ TAILLE,
				(int) ((x2 - minX) / taille.getLongitude() * this.getWidth())
						+ TAILLE,
				(int) ((y2 - minY) / taille.getLatitude() * this.getHeight())
						+ TAILLE);
	}

	@Override
	public void componentHidden(ComponentEvent e) {

	}

	@Override
	public void componentMoved(ComponentEvent e) {

	}

	@Override
	public void componentResized(ComponentEvent e) {
		this.aInitialiser = true;
	}

	@Override
	public void componentShown(ComponentEvent e) {
	}

	/**
	 * Permet d'initialiser ou reinitialiser le cadre a dessiner pour que tout
	 * le modèle soit vu sur le panel
	 */
	private void initialiserPanel() {
		Dimensions dim = controleur.getDimensions();
		double ratio = (double) this.getWidth() / (double) this.getHeight();

		minX = dim.getPositionMinimum().getLongitude();
		minY = dim.getPositionMinimum().getLatitude();
		taille = dim.getTaille();

		if (taille.getLongitude() > taille.getLatitude() * ratio) {
			minY -= (taille.getLongitude() / ratio - taille.getLatitude()) / 2;
			taille.setLatitude(taille.getLongitude() / ratio);
		} else {
			minX -= (taille.getLatitude() * ratio - taille.getLongitude()) / 2;
			taille.setLongitude(taille.getLatitude() * ratio);
		}
		repaint();
		aInitialiser = false;
	}
}
