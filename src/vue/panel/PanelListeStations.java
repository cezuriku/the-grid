package vue.panel;

import java.awt.Color;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class PanelListeStations extends JScrollPane {
	private JTable tableStations;

	/**
	 * Constructeur par défaut
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 */
	public PanelListeStations(InterfaceControleur controleur) {
		TableModel listeStations = controleur.getListeStations();
		tableStations = new JTable(listeStations);
		tableStations.setBackground(new Color(0, 8, 8));
		//tableStations.setGridColor(new Color(255, 51, 18));
		tableStations.setForeground(new Color(87, 146, 245));

		tableStations.setVisible(true);
		this.getViewport().add(tableStations);
	}
}
