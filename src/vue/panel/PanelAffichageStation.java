package vue.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JPanel;

import modele.Dimensions;
import modele.Position;
import modele.Station;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class PanelAffichageStation extends JPanel {
	private InterfaceControleur controleur;

	/**
	 * Constructeur par défaut
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 */
	public PanelAffichageStation(InterfaceControleur controleur) {
		this.controleur = controleur;

		if (!controleur.modeleCharge()) {
			try {
				controleur.chargerModele("recherches/stations.txt");
			} catch (IOException ex) {
				System.out.println(ex.toString());
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		if (controleur.modeleCharge()) {
			Dimensions dim = controleur.getDimensions();
			Position taille = dim.getTaille();
			double tailleX = taille.getLongitude();
			double tailleY = taille.getLatitude();
			double minX = dim.getPositionMinimum().getLongitude();
			double minY = dim.getPositionMinimum().getLatitude();
			double x;
			double y;

			g.setColor(Color.BLACK);

			HashMap<Integer, Station> listesStations = controleur.getStations();

			for (Entry<Integer, Station> mapStation : listesStations.entrySet()) {
				Station station = mapStation.getValue();
				x = station.getPosition().getLongitude();
				y = station.getPosition().getLatitude();

				g.drawOval((int) ((x - minX) / tailleX * this.getWidth()),
						(int) ((y - minY) / tailleY * this.getHeight()), 5, 5);
			}

			g.dispose();
		}

	}
}
