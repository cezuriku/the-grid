package vue.panel.formes;

import modele.Position;
import modele.Station;

/**
 * Cette classe représente un Carre qui contient une station il est utile au
 * drag & drop de la vue
 * 
 * @see Station
 */
public class Carre {

	private Station station;

	private Position decalage;
	private Position nouvellePosition;

	/**
	 * Constructeur par défaut de Carre
	 * 
	 * @param station
	 *            La station a modifier
	 * @param position
	 *            La position du curseur
	 */
	public Carre(Station station, Position position) {
		this.station = station;
		this.decalage = Position.soustraction(position, station.getPosition());
		this.nouvellePosition = station.getPosition();
	}

	/**
	 * Getter Position
	 * 
	 * @return La nouvelle position de la station
	 */
	public Position getPosition() {
		return nouvellePosition;
	}

	/**
	 * Setter Position
	 * 
	 * @param position
	 *            change la position actuelle de la station
	 */
	public void setPosition(Position position) {
		this.nouvellePosition = Position.soustraction(position, decalage);
	}

	/**
	 * Affecter a la station la nouvelle Position enregistré
	 */
	public void valider() {
		station.setPosition(nouvellePosition);
	}

}
