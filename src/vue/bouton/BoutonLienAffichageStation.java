package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionEvent;

import vue.ecran.EcranAffichageStation;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class BoutonLienAffichageStation extends BoutonLien {

	/**
	 * Hérite de BoutonLien. Fait un lien vers l'affichage de la Carte
	 * 
	 * @see BoutonLien
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLienAffichageStation(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.setText("Affichage Carte");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Redirection vers l'affichage de la carte");
		controleur.setEcran(new EcranAffichageStation(controleur, conteneur));
	}
}
