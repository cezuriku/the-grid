package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public abstract class BoutonLien extends JButton implements ActionListener {
	protected InterfaceControleur controleur;
	protected Container conteneur;

	/**
	 * Constructeur par défaut du BoutonLien. Ce bouton permet d'avoir le
	 * controleur et le conteneur dès l'initialisation
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLien(InterfaceControleur controleur, Container conteneur) {
		this.controleur = controleur;
		this.conteneur = conteneur;
		this.setVisible(true);
		this.addActionListener(this);
	}
}
