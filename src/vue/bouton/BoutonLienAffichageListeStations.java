package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionEvent;

import vue.ecran.EcranAffichageListeStations;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class BoutonLienAffichageListeStations extends BoutonLien {

	/**
	 * Hérite de BoutonLien. Fait un lien vers l'affichage de la liste des stations.
	 * 
	 * @see BoutonLien
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLienAffichageListeStations(InterfaceControleur controleur,
			Container conteneur) {
		super(controleur, conteneur);
		this.setText("Liste Stations");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Redirection vers l'affichage de liste stations");
		controleur.setEcran(new EcranAffichageListeStations(controleur,
				conteneur));
	}
}
