package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionEvent;

import vue.ecran.EcranAjouterStation;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class BoutonLienAjouterStation extends BoutonLien {

	/**
	 * Hérite de BoutonLien. Fait un lien vers l'affichage de l'ajout de station
	 * 
	 * @see BoutonLien
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLienAjouterStation(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.setText("Ajouter station");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Redirection vers l'ajout de station");
		controleur.setEcran(new EcranAjouterStation(controleur, conteneur));
	}
}