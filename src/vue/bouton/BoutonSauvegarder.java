package vue.bouton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class BoutonSauvegarder extends JButton implements ActionListener {
	protected InterfaceControleur controleur;

	/**
	 * Sauvegarde le modèle
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 */
	public BoutonSauvegarder(InterfaceControleur controleur) {
		this.controleur = controleur;
		this.setVisible(true);
		this.addActionListener(this);
		this.setText("Sauvegarder");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("Sauvegarde du modèle");
		controleur.sauvegarderModele();
	}
}
