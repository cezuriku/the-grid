package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionEvent;

import vue.ecran.EcranAffichageLigne;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class BoutonLienAffichageLigne extends BoutonLien {

	/**
	 * Hérite de BoutonLien. Fait un lien vers l'affichage des lignes.
	 * 
	 * @see BoutonLien
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLienAffichageLigne(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.setText("Affichage Ligne");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Redirection vers l'affichages des lignes");
		controleur.setEcran(new EcranAffichageLigne(controleur, conteneur));
	}
}
