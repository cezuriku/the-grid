package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionEvent;

import vue.ecran.EcranMenu;
import controleur.InterfaceControleur;
/**
 * 
 * @deprecated
 *
 */
@SuppressWarnings("serial")
public class BoutonLienMenu extends BoutonLien {

	/**
	 * Hérite de BoutonLien. Fait un lien vers l'affichage du menu
	 * 
	 * @see BoutonLien
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLienMenu(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.setText("Retour au menu");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Redirection vers le menu");
		controleur.setEcran(new EcranMenu(controleur, conteneur));
	}
}
