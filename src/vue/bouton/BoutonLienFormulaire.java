package vue.bouton;

import java.awt.Container;
import java.awt.event.ActionEvent;

import vue.ecran.EcranAffichageFormulaire;
import controleur.InterfaceControleur;

@SuppressWarnings("serial")
public class BoutonLienFormulaire extends BoutonLien {

	/**
	 * Hérite de BoutonLien. Fait un lien vers l'affichage du formulaire
	 * 
	 * @see BoutonLien
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public BoutonLienFormulaire(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.setText("Affichage Formulaire");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Redirection vers le formulaire");
		controleur.setEcran(new EcranAffichageFormulaire(controleur, conteneur));
	}
}
