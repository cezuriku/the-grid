package vue;

import vue.ecran.Ecran;
import controleur.InterfaceControleur;

public interface InterfaceVue {

	/**
	 * Ajoute le controleur a la classe
	 * 
	 * @param controleur
	 */
	public void ajouterControleur(InterfaceControleur controleur);

	/**
	 * Permet de lancer la fenêtre
	 */
	public void lancer();

	/**
	 * Change l'écran à afficher
	 * 
	 * @param ecran
	 *            Le nouvel ecran
	 */
	public void modifierEcran(Ecran ecran);

}
