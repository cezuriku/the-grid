package vue.ecran;

import java.awt.BorderLayout;
import java.awt.Container;

import vue.panel.PanelAffichageLigne;
import vue.panel.PanelBoutonMenu;
import controleur.InterfaceControleur;

public class EcranAffichageLigne extends Ecran {
	PanelAffichageLigne affichageLigne;

	/**
	 * Hérite de Ecran. Ecran affichant les lignes
	 * 
	 * @see Ecran
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public EcranAffichageLigne(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.affichageLigne = new PanelAffichageLigne(controleur);
		conteneur.setLayout(new BorderLayout());

		conteneur.addComponentListener(affichageLigne);
		conteneur.add(affichageLigne, BorderLayout.CENTER);
		
		conteneur.add(new PanelBoutonMenu(controleur, conteneur), BorderLayout.SOUTH);
	}
	
	public void supprimerListeners() {
		conteneur.removeComponentListener(affichageLigne);
	}

}
