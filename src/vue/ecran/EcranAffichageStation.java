package vue.ecran;

import java.awt.BorderLayout;
import java.awt.Container;

import vue.panel.PanelAffichageStation2;
import vue.panel.PanelBoutonMenu;
import controleur.InterfaceControleur;

public class EcranAffichageStation extends Ecran {
	PanelAffichageStation2 affichageStation;

	/**
	 * Hérite de Ecran. Ecran affichant la carte
	 * 
	 * @see Ecran
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public EcranAffichageStation(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		this.affichageStation = new PanelAffichageStation2(controleur);
		conteneur.setLayout(new BorderLayout());
		conteneur.addMouseListener(affichageStation);
		conteneur.addMouseMotionListener(affichageStation);
		conteneur.addComponentListener(affichageStation);
		
		conteneur.add(affichageStation, BorderLayout.CENTER);
		
		conteneur.add(new PanelBoutonMenu(controleur, conteneur), BorderLayout.SOUTH);
	}
	
	public void supprimerListeners() {
		conteneur.removeMouseListener(affichageStation);
		conteneur.removeMouseMotionListener(affichageStation);
		conteneur.removeComponentListener(affichageStation);
	}

}
