package vue.ecran;

import java.awt.BorderLayout;
import java.awt.Container;

import vue.panel.PanelBoutonMenu;
import controleur.InterfaceControleur;

public class EcranAffichageFormulaire extends Ecran {

	/**
	 * Hérite de Ecran. Ecran affichant le formulaire pour les ajouts de stations
	 * 
	 * @see Ecran
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public EcranAffichageFormulaire(InterfaceControleur controleur,
			Container conteneur) {
		super(controleur, conteneur);
		conteneur.setLayout(new BorderLayout());
		conteneur.add(new PanelBoutonMenu(controleur, conteneur),
				BorderLayout.SOUTH);
	}

	@Override
	public void supprimerListeners() {

	}
}
