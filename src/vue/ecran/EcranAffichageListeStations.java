package vue.ecran;

import java.awt.BorderLayout;
import java.awt.Container;

import vue.panel.PanelBoutonMenu;
import vue.panel.PanelListeStations;
import controleur.InterfaceControleur;

public class EcranAffichageListeStations extends Ecran {

	/**
	 * Hérite de Ecran. Ecran affichant la liste des stations
	 * 
	 * @see Ecran
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public EcranAffichageListeStations(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		conteneur.setLayout(new BorderLayout());
		conteneur.add(new PanelListeStations(controleur), BorderLayout.CENTER);
		conteneur.add(new PanelBoutonMenu(controleur, conteneur), BorderLayout.SOUTH);
	}

	@Override
	public void supprimerListeners() {
		
	}
}
