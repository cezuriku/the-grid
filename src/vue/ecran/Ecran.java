package vue.ecran;

import java.awt.Container;

import controleur.InterfaceControleur;

/**
 * Ecran
 */
public abstract class Ecran {
	
	protected InterfaceControleur controleur;
	protected Container conteneur;
	
	/**
	 * Créé un ecran avec un controleur et un conteneur
	 * 
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public Ecran(InterfaceControleur controleur, Container conteneur) {
		this.conteneur = conteneur;
		this.controleur = controleur;
		this.conteneur.removeAll();
	}

	/**
	 * Change le controleur
	 * 
	 * @param controleur
	 */
	public void setControleur(InterfaceControleur controleur) {
		this.controleur = controleur;
	}

	/**
	 * Change le conteneur
	 * 
	 * @param conteneur
	 */
	public void setConteneur(Container conteneur) {
		this.conteneur = conteneur;
	}

	/**
	 * Supprime tout les listeners que l'ecran aura réclamé
	 */
	public abstract void supprimerListeners();

}
