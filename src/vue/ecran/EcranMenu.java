package vue.ecran;

import java.awt.Container;
import java.awt.GridLayout;

import vue.bouton.BoutonLienAffichageStation;
import vue.bouton.BoutonLienFormulaire;
import controleur.InterfaceControleur;
/**
 * @deprecated
 *
 */
public class EcranMenu extends Ecran {

	/**
	 * Hérite de Ecran. Ecran de menu
	 * 
	 * @see Ecran
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public EcranMenu(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		GridLayout disposition = new GridLayout(3, 1);
		disposition.setVgap(42);
		conteneur.setLayout(disposition);
		conteneur.add(new BoutonLienAffichageStation(controleur, conteneur));
		conteneur.add(new BoutonLienFormulaire(controleur, conteneur));
		
	}

	@Override
	public void supprimerListeners() {
		
	}

}
