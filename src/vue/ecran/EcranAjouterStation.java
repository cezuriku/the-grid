package vue.ecran;

import java.awt.BorderLayout;
import java.awt.Container;

import vue.panel.PanelAjouterStation;
import vue.panel.PanelBoutonMenu;
import controleur.InterfaceControleur;

public class EcranAjouterStation extends Ecran {

	/**
	 * Hérite de Ecran. Ecran permettant d'ajouter une station
	 * 
	 * @see Ecran
	 * @param controleur
	 *            Le controleur sur lequel on envoie l'action
	 * @param conteneur
	 *            Le conteneur sur lequel on peux dessiner
	 */
	public EcranAjouterStation(InterfaceControleur controleur, Container conteneur) {
		super(controleur, conteneur);
		conteneur.setLayout(new BorderLayout());
		conteneur.add(new PanelAjouterStation(controleur, conteneur), BorderLayout.CENTER);
		conteneur.add(new PanelBoutonMenu(controleur, conteneur), BorderLayout.SOUTH);
	}

	@Override
	public void supprimerListeners() {
		
	}

}