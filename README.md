The Grid est un projet que nous réalisons dans le cadre de notre cursus à l'Institut Universitaire de Technologie informatique de Caen.

Il s'agit de réaliser un logiciel de cartographie faisant figurer des stations (métro, tram, trains, ou autre) et des lignes liant ces stations entre elles. L'utilisateur doit pouvoir modifier la carte (rajout de stations, tracé des lignes) aisément.